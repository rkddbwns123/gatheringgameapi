package com.kyjg.gatheringgameapi.repository;

import com.kyjg.gatheringgameapi.entity.UserCollection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserCollectionRepository extends JpaRepository<UserCollection, Long> {
    Optional<UserCollection> findByGameItemStat_Id(long id);

    List<UserCollection> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
}
