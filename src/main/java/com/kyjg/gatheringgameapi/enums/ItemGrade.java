package com.kyjg.gatheringgameapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ItemGrade {
    COMMON("흔함"),
    UNCOMMON("안흔함"),
    SPECIAL("특별함"),
    UNIQUE("희귀함"),
    LEGENDARY("전설적인");

    private final String gradeName;
}
