package com.kyjg.gatheringgameapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpenBoxPercentItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "확률 최소값")
    private Double percentMin;
    @ApiModelProperty(notes = "확률 최댓값")
    private Double percentMax;
}
