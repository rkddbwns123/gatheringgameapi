package com.kyjg.gatheringgameapi.model;

import com.kyjg.gatheringgameapi.entity.UserCollection;
import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserCollectionItem {
    @ApiModelProperty(notes = "이미지 명")
    private String imageName;
    @ApiModelProperty(notes = "아이템 명")
    private String itemName;
    @ApiModelProperty(notes = "등급 값")
    private String itemGrade;
    @ApiModelProperty(notes = "등급 명")
    private String itemGradeName;
    @ApiModelProperty(notes = "수량")
    private Integer itemCount;

    private UserCollectionItem(UserCollectionItemBuilder builder) {
        this.imageName = builder.imageName;
        this.itemName = builder.itemName;
        this.itemGrade = builder.itemGrade;
        this.itemGradeName = builder.itemGradeName;
        this.itemCount = builder.itemCount;

    }

    public static class UserCollectionItemBuilder implements CommonModelBuilder<UserCollectionItem> {
        private final String imageName;
        private final String itemName;
        private final String itemGrade;
        private final String itemGradeName;
        private final Integer itemCount;

        public UserCollectionItemBuilder(UserCollection collection) {
            this.imageName = collection.getGameItemStat().getImageFileName();
            this.itemName = collection.getGameItemStat().getItemName();
            this.itemGrade = collection.getGameItemStat().getItemGrade().toString();
            this.itemGradeName = collection.getGameItemStat().getItemGrade().getGradeName();
            this.itemCount = collection.getItemCount();
        }

        @Override
        public UserCollectionItem build() {
            return new UserCollectionItem(this);
        }
    }
}
