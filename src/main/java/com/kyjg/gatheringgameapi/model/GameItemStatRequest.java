package com.kyjg.gatheringgameapi.model;

import com.kyjg.gatheringgameapi.enums.ItemGrade;
import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GameItemStatRequest {
    @ApiModelProperty(notes = "등급 명")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ItemGrade itemGrade;
    @ApiModelProperty(notes = "아이템 명")
    @NotNull
    @Length(min = 1, max = 20)
    private String itemName;
    @ApiModelProperty(notes = "나무 상자 확률")
    @NotNull
    @Min(value = 0)
    @Max(value = 100)
    private Double woodBoxPercent;
    @ApiModelProperty(notes = "좋아 보이는 상자 확률")
    @NotNull
    @Min(value = 0)
    @Max(value = 100)
    private Double goodBoxPercent;
    @ApiModelProperty(notes = "이미지 파일 명")
    @NotNull
    @Length(min = 1, max = 50)
    private String imageFileName;

    private GameItemStatRequest(GameItemStatRequestBuilder builder) {
        this.itemGrade = builder.itemGrade;
        this.itemName = builder.itemName;
        this.woodBoxPercent = builder.woodBoxPercent;
        this.goodBoxPercent = builder.goodBoxPercent;
        this.imageFileName = builder.imageFileName;
    }

    public static class GameItemStatRequestBuilder implements CommonModelBuilder<GameItemStatRequest> {

        private final ItemGrade itemGrade;
        private final String itemName;
        private final Double woodBoxPercent;
        private final Double goodBoxPercent;
        private final String imageFileName;

        public GameItemStatRequestBuilder(ItemGrade itemGrade, String itemName, Double woodBoxPercent, Double goodBoxPercent, String imageFileName) {
            this.itemGrade = itemGrade;
            this.itemName = itemName;
            this.woodBoxPercent = woodBoxPercent;
            this.goodBoxPercent = goodBoxPercent;
            this.imageFileName = imageFileName;
        }

        @Override
        public GameItemStatRequest build() {
            return new GameItemStatRequest(this);
        }
    }
}
