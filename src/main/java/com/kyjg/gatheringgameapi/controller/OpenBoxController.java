package com.kyjg.gatheringgameapi.controller;

import com.kyjg.gatheringgameapi.model.OpenBoxResultResponse;
import com.kyjg.gatheringgameapi.model.SingleResult;
import com.kyjg.gatheringgameapi.service.OpenBoxService;
import com.kyjg.gatheringgameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "상자 열기")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/open-box")
public class OpenBoxController {
    private final OpenBoxService openBoxService;

    @ApiOperation(value = "나무 상자 열기")
    @PostMapping("/wood")
    public SingleResult<OpenBoxResultResponse> openWoodBox() {
        return ResponseService.getSingleResult(openBoxService.getResult(true));
    }

    @ApiOperation(value = "좋아 보이는 상자 열기")
    @PostMapping("/good")
    public SingleResult<OpenBoxResultResponse> openGoodBox() {
        return ResponseService.getSingleResult(openBoxService.getResult(false));
    }
}
