package com.kyjg.gatheringgameapi.controller;

import com.kyjg.gatheringgameapi.model.CommonResult;
import com.kyjg.gatheringgameapi.model.FirstConnectDataResponse;
import com.kyjg.gatheringgameapi.model.SingleResult;
import com.kyjg.gatheringgameapi.service.GameDataService;
import com.kyjg.gatheringgameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "게임 데이터 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/game-data")
public class GameDataController {
    private final GameDataService gameDataService;

    @ApiOperation(value = "첫 접속 시 데이터 불러오기")
    @GetMapping("/init")
    public SingleResult<FirstConnectDataResponse> getFirstData() {
        return ResponseService.getSingleResult(gameDataService.getFirstData());
    }
    @ApiOperation(value = "게임 리셋")
    @DeleteMapping("/reset")
    public CommonResult gameReset() {
        gameDataService.gameReset();

        return ResponseService.getSuccessResult();
    }
}
