package com.kyjg.gatheringgameapi.service;

import com.kyjg.gatheringgameapi.entity.GameItemStat;
import com.kyjg.gatheringgameapi.entity.UserCollection;
import com.kyjg.gatheringgameapi.entity.UserStat;
import com.kyjg.gatheringgameapi.enums.ItemGrade;
import com.kyjg.gatheringgameapi.model.GameItemStatRequest;
import com.kyjg.gatheringgameapi.repository.GameItemStatRepository;
import com.kyjg.gatheringgameapi.repository.UserCollectionRepository;
import com.kyjg.gatheringgameapi.repository.UserStatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InitDataService {
    private final UserStatRepository userStatRepository;
    private final UserCollectionRepository userCollectionRepository;
    private final GameItemStatRepository gameItemStatRepository;

    public void setFirstMoney() {
        Optional<UserStat> originData = userStatRepository.findById(1L);

        if (originData.isEmpty()) {
            UserStat addData = new UserStat.userStatBuilder().build();

            userStatRepository.save(addData);
        }
    }
    public void setFirstGameItem() {
        List<GameItemStat> originList = gameItemStatRepository.findAll();

        if (originList.size() == 0) {
            List<GameItemStatRequest> result = new LinkedList<>();

            GameItemStatRequest request1 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.COMMON, "뼈다귀", 10D, 5D, "bone.png").build();
            result.add(request1);
            GameItemStatRequest request2 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.COMMON, "정체 모를 가루", 10D, 5D, "sugar.png").build();
            result.add(request2);
            GameItemStatRequest request3 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.COMMON, "발광석 가루", 10D, 5D, "light.png").build();
            result.add(request3);
            GameItemStatRequest request4 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.COMMON, "레드스톤 가루", 10D, 5D, "redstone.png").build();
            result.add(request4);
            GameItemStatRequest request5 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.COMMON, "화약 가루", 10D, 5D, "gunpowder.png").build();
            result.add(request5);
            GameItemStatRequest request6 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNCOMMON, "석탄", 6D, 8D, "coal.png").build();
            result.add(request6);
            GameItemStatRequest request7 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNCOMMON, "숯", 6D, 8D, "charcoal.png").build();
            result.add(request7);
            GameItemStatRequest request8 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNCOMMON, "구리", 6D, 8D, "copper.png").build();
            result.add(request8);
            GameItemStatRequest request9 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNCOMMON, "부싯돌", 6D, 8D, "flint.png").build();
            result.add(request9);
            GameItemStatRequest request10 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNCOMMON, "벽돌", 6D, 8D, "brick.png").build();
            result.add(request10);
            GameItemStatRequest request11 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.SPECIAL, "수정", 3D, 5D, "crystal.png").build();
            result.add(request11);
            GameItemStatRequest request12 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.SPECIAL, "석영", 3D, 5D, "quartz.png").build();
            result.add(request12);
            GameItemStatRequest request13 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.SPECIAL, "은", 3D, 5D, "silver.png").build();
            result.add(request13);
            GameItemStatRequest request14 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.SPECIAL, "금", 3D, 5D, "gold.png").build();
            result.add(request14);
            GameItemStatRequest request15 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.SPECIAL, "루비", 3D, 5D, "ruby.png").build();
            result.add(request15);
            GameItemStatRequest request16 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNIQUE, "청금석", 1.2, 2.25, "lazurite.png").build();
            result.add(request16);
            GameItemStatRequest request17 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNIQUE, "자수정", 1.2, 2.25, "amethyst.png").build();
            result.add(request17);
            GameItemStatRequest request18 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNIQUE, "에메랄드", 1.2, 2.25, "emerald.png").build();
            result.add(request18);
            GameItemStatRequest request19 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.UNIQUE, "다이아몬드", 1.2, 2.25, "diamond.png").build();
            result.add(request19);
            GameItemStatRequest request20 = new GameItemStatRequest.GameItemStatRequestBuilder(
                    ItemGrade.LEGENDARY, "전설의 광석", 0.2, 1D, "legendary.png").build();
            result.add(request20);

            result.forEach(item -> {
                GameItemStat addData = new GameItemStat.gameItemStatBuilder(item).build();

                gameItemStatRepository.save(addData);
            });
        }
    }
    public void setFirstUserCollection() {
        List<UserCollection> userCollections = userCollectionRepository.findAll();

        if (userCollections.size() == 0) {
            List<GameItemStat> gameItemStats = gameItemStatRepository.findAll();

            gameItemStats.forEach(item -> {
                UserCollection addData = new UserCollection.UserCollectionBuilder(item).build();

                userCollectionRepository.save(addData);
            });
        }
    }
}
