package com.kyjg.gatheringgameapi.service;

import com.kyjg.gatheringgameapi.entity.UserCollection;
import com.kyjg.gatheringgameapi.entity.UserStat;
import com.kyjg.gatheringgameapi.exception.CMissingDataException;
import com.kyjg.gatheringgameapi.model.FirstConnectDataResponse;
import com.kyjg.gatheringgameapi.model.UserCollectionItem;
import com.kyjg.gatheringgameapi.model.UserStatResponse;
import com.kyjg.gatheringgameapi.repository.UserCollectionRepository;
import com.kyjg.gatheringgameapi.repository.UserStatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameDataService {
    private final UserStatRepository userStatRepository;
    private final UserCollectionRepository userCollectionRepository;

    public FirstConnectDataResponse getFirstData() {
        FirstConnectDataResponse result = new FirstConnectDataResponse();

        UserStat userStat = userStatRepository.findById(1L).orElseThrow(CMissingDataException::new);

        result.setUserStatResponse(new UserStatResponse.UserStatResponseBuilder(userStat).build());

        result.setUserCollectionItems(getMyItems());

        return result;
    }

    public List<UserCollectionItem> getMyItems() {
        List<UserCollection> originList = userCollectionRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<UserCollectionItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UserCollectionItem.UserCollectionItemBuilder(item).build()));

        return result;
    }
    public void gameReset() {
        UserStat userStat = userStatRepository.findById(1L).orElseThrow(CMissingDataException::new);
        userStat.resetMoneyCount();
        userStatRepository.save(userStat);

        List<UserCollection> originList = userCollectionRepository.findAll();
        for (UserCollection item : originList) {
            item.resetItemCount();
            userCollectionRepository.save(item);
        }
    }
}
