package com.kyjg.gatheringgameapi.service;

import com.kyjg.gatheringgameapi.entity.UserStat;
import com.kyjg.gatheringgameapi.exception.CMissingDataException;
import com.kyjg.gatheringgameapi.model.UserStatResponse;
import com.kyjg.gatheringgameapi.repository.UserStatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserStatService {
    private final UserStatRepository userStatRepository;

    public UserStatResponse putMoneyCountPlus() {
        UserStat money = userStatRepository.findById(1L).orElseThrow(CMissingDataException::new);

        money.plusMoneyCount();
        UserStat result = userStatRepository.save(money);

        return new UserStatResponse.UserStatResponseBuilder(result).build();
    }
}
