package com.kyjg.gatheringgameapi.entity;

import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserStat {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "보유 금액")
    @Column(nullable = false)
    private Long moneyCount;

    public void plusMoneyCount() {
        this.moneyCount += 100;
    }

    public void minusMoneyCount(long boxPay) {
        this.moneyCount -= boxPay;
    }
    public void resetMoneyCount() {
        this.moneyCount = 0L;
    }

    private UserStat(userStatBuilder builder) {
        this.moneyCount = builder.moneyCount;

    }

    public static class userStatBuilder implements CommonModelBuilder<UserStat> {

        private final Long moneyCount;

        public userStatBuilder() {
            this.moneyCount = 0L;
        }
        @Override
        public UserStat build() {
            return new UserStat(this);
        }
    }
}
