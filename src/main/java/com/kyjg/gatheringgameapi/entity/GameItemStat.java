package com.kyjg.gatheringgameapi.entity;

import com.kyjg.gatheringgameapi.enums.ItemGrade;
import com.kyjg.gatheringgameapi.interfaces.CommonModelBuilder;
import com.kyjg.gatheringgameapi.model.GameItemStatRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameItemStat {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "등급 명")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private ItemGrade itemGrade;
    @ApiModelProperty(notes = "아이템 명")
    @Column(nullable = false, length = 20)
    private String itemName;
    @ApiModelProperty(notes = "나무 상자 확률")
    @Column(nullable = false)
    private Double woodBoxPercent;
    @ApiModelProperty(notes = "좋아 보이는 상자 확률")
    @Column(nullable = false)
    private Double goodBoxPercent;
    @ApiModelProperty(notes = "이미지 파일 명")
    @Column(nullable = false, length = 50)
    private String imageFileName;

    private GameItemStat(gameItemStatBuilder builder) {
        this.itemGrade = builder.itemGrade;
        this.itemName = builder.itemName;
        this.woodBoxPercent = builder.woodBoxPercent;
        this.goodBoxPercent = builder.goodBoxPercent;
        this.imageFileName = builder.imageFileName;

    }

    public static class gameItemStatBuilder implements CommonModelBuilder<GameItemStat> {

        private final ItemGrade itemGrade;
        private final String itemName;
        private final Double woodBoxPercent;
        private final Double goodBoxPercent;
        private final String imageFileName;

        public gameItemStatBuilder(GameItemStatRequest request) {
            this.itemGrade = request.getItemGrade();
            this.itemName = request.getItemName();
            this.woodBoxPercent = request.getWoodBoxPercent();
            this.goodBoxPercent = request.getGoodBoxPercent();
            this.imageFileName = request.getImageFileName();
        }
        @Override
        public GameItemStat build() {
            return new GameItemStat(this);
        }
    }


}
