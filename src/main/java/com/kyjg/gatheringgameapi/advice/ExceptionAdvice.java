package com.kyjg.gatheringgameapi.advice;

import com.kyjg.gatheringgameapi.enums.ResultCode;
import com.kyjg.gatheringgameapi.exception.CMissingDataException;
import com.kyjg.gatheringgameapi.exception.CNotEnoughMoneyException;
import com.kyjg.gatheringgameapi.exception.CWrongPhoneNumberException;
import com.kyjg.gatheringgameapi.model.CommonResult;
import com.kyjg.gatheringgameapi.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CNotEnoughMoneyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotEnoughMoneyException e) {
        return ResponseService.getFailResult(ResultCode.NOT_ENOUGH_MONEY);
    }

}
